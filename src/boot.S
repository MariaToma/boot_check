    bits 16

    mov ax, 0x07C0
    mov ds, ax
    mov ax, 0x07E0      ; 07E0h = (07C00h+200h)/10h, beginning of stack segment.
    mov ss, ax
    mov sp, 0x2000      ; 8k of stack space.

    call clearscreen

    push 0x0000
    call movecursor
    add sp, 2

    push string_Hello_there
    call print
    add sp, 2
	mov bl, 80h

start_print_drives:
    mov ah, 08h
	mov dl, bl

	int 13h
	jc end_print_drives

	push string_drive_found
	call print
	add sp, 2

print_info:
	mov ah, 08h
	mov dl, bl

	int 13h
	jc end_print_drives

; Read sectors per track
	mov ax, cx
	and ax, 0x1f
	call print_ax

	push string_sectors_per_track
	call print
	add sp,2

	mov ax, cx
	and ax, 0xffc0
	shr ax, 6
	add ax, 1              ; number_of - 1 (because index starts with 0)
	call print_ax

	push string_cylinders
	call print
	add sp,2

	mov al, dh
	mov ah, 0
	add ax, 1              ; number_of - 1 (because index starts with 0)

	call print_ax

	push string_heads
	call print
	add sp, 2

	inc bl
	jmp start_print_drives
	
end_print_drives:

	push string_done
	call print
	add sp,2



    cli
    hlt

	


    clearscreen:
        push bp
        mov bp, sp
        pusha

        mov ah, 0x07        ; tells BIOS to scroll down window
        mov al, 0x00        ; clear entire window
        mov bh, 0x07        ; white on black
        mov cx, 0x00        ; specifies top left of screen as (0,0)
        mov dh, 0x18        ; 18h = 24 rows of chars
        mov dl, 0x4f        ; 4fh = 79 cols of chars
        int 0x10            ; calls video interrupt




        popa                ; pops all the general purpose registers
        mov sp, bp
        pop bp
        ret

    movecursor:
        push bp
        mov bp, sp
        pusha

        mov dx, [bp+4]      ; get the argument from the stack. |bp| = 2, |arg| = 2
        mov ah, 0x02        ; set cursor position
        mov bh, 0x00        ; page 0 - doesn't matter, we're not using double-buffering
        int 0x10

        popa
        mov sp, bp
        pop bp
        ret

	print_ax:
		push si
		push cx
		mov si, number
		add si, 9
		mov dx, 0

	print_ax_middle:
		mov cx, 10
		div cx
		add dx, 0x30
		mov [si], dl
		mov dx, 0
		dec si
		or ax,ax
		jnz print_ax_middle
		
		push number
		call print
		add sp, 2

		mov si, number
		add si, 9
		mov al, 0x20
		mov cx, 10
	clear_number:
		mov [si], al
		dec si
		loop clear_number

		
		pop cx
		pop si
		ret

    print:
        push bp
        mov bp, sp
        pusha
        mov si, [bp+4]      ; grab the pointer to the data
        mov bh, 0x00        ; page number, 0 again
        mov bl, 0x00        ; foreground color, irrelevant - in text mode
        mov ah, 0x0E        ; print character to TTY
    .char:
        mov al, [si]        ; get the current char from our pointer position
        add si, 1           ; keep incrementing si until we see a null char
        or al, 0
        je .return          ; end if the string is done
        int 0x10            ; print the character if we're not done
        jmp .char           ; keep looping
    .return:
        popa
        mov sp, bp
        pop bp
        ret
	string_done: db "Done!", 0xd, 0xa, 0
	number: db "          ", 0
    string_Hello_there:    db "Hello there!", 0xd, 0xa,0
	string_drive_found: db "Drive found!", 0xd, 0xa,0
	string_sectors_per_track: db "sec/track", 0xd, 0xa,0
	string_cylinders: db "cylinders", 0xd, 0xa,0
	string_heads: db "heads", 0xd, 0xa,0

times 510-($-$$) db 0
dw 0xAA55
